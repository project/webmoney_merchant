<?php

function webmoney_merchant_prefillform($form_state){
   
	$form=array(); $currencies_ar=array();

	$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));
	foreach($currency_settings as $key=>$value){
		if($value['enabled']==1) $currencies_ar[$key]=$key;
	}

	
	$form['currency'] = array(
		'#type' => 'radios',
		'#title' => t("Currency"),
		'#options' => $currencies_ar,
		'#default_value' => 'USD',
		//'#description' => t("Select payment currency."),
		'#required' => TRUE
	);
 

	$form['amount'] = array(
		'#type' => 'textfield', 
		'#title' => t('Amount'), 
		'#default_value' => '', 
		'#size' => 10, 
		'#maxlength' => 12, 
		'#required' => TRUE
	);

	$form['memo'] = array(
		'#type' => 'textarea', 
		'#title' => t('Memo'), 
		'#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))), 
		'#description' => t("Payment description."),
		'#required' => TRUE
	);


	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Cerate payment'),
	);

	return $form;

}

function webmoney_merchant_prefillform_submit(&$form, $form_state){
	
	$payment=_webmoney_merchant_createpayment(array(
		'amount'=>$form_state['values']['amount'],
		'currency'=>$form_state['values']['currency'],
		'memo'=>$form_state['values']['memo'],
	));


	if(is_array($payment) && $payment['pid']>0){
		drupal_set_message("Please confirm payment details");
		drupal_goto('webmoney/payment/'.$payment['pid']);
	}

}

function webmoney_merchant_merchantform($form_state, $payment){

	if(!is_array($payment) && is_integer($payment)){ // fetch payment info from DB
		$payment=webmoney_merchant_pid_load($payment);
	}

	if(!is_array($payment) || !($payment['pid']>0)){

		$form['error'] = array(
		  '#type' => 'item',
		  '#title' => t('Error'),
		  '#value' => t('Order you are going to pay for does not exist'),
		);

	}else{

		$form['#action'] = variable_get('webmoney_merchant_payment_url', WEBMONEY_MERCHANT_PAYMENT_URL);
		
		$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));
		$presc = $currency_settings["{$payment['currency']}"]['presc'];

		// interface data:

		$form['payment_id'] = array(
		  '#type' => 'item',
		  '#title' => t('Order #'),
		  '#value' => $payment['pid'],
		);

		$form['amount'] = array(
		  '#type' => 'item',
		  '#title' => t('Amount'),
		  '#value' => round($payment['amount'], $presc).' '.$payment['currency'],
		);

		$form['memo'] = array(
		  '#type' => 'item',
		  '#title' => t('Memo'),
		  '#value' => $payment['memo']
		);

		// merchant data...
	   
		$form['LMI_PAYEE_PURSE'] = array(
			'#type' => 'hidden',
			'#value' => $payment['payee_account'],
		);

		$form['LMI_PAYMENT_NO'] = array(
			'#type' => 'hidden',
			'#value' => $payment['pid'],
		);

	 
		$form['LMI_PAYMENT_DESC'] = array(
			'#type' => 'hidden',
			'#value' => $payment['memo'],
		);

		$form['LMI_PAYMENT_AMOUNT'] = array(
			'#type' => 'hidden',
			'#value' => round($payment['amount'], $presc),
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Pay now'),
		);

	}

	return $form;

}

function webmoney_merchant_success(){

	return theme('webmoney_merchant_success');

}

function webmoney_merchant_fail(){
	return theme('webmoney_merchant_fail');
}

function webmoney_merchant_status(){

	drupal_set_header('Content-type: text/html; charset=iso-8859-1');

	$LMI_MODE=0; // 1 = test mode, 0 = real mode
	$debug_errors=false;

	$created=time();

	// ----- Prerequest:
	if (isset ( $_POST ['LMI_PREREQUEST'] ) && $_POST ['LMI_PREREQUEST'] == 1) { 

		// check url
		$url=trim($_SERVER['REQUEST_URI'], '/');
		$alias=drupal_get_path_alias('webmoney/status');
		if($url!=$alias) die('NO');

		if (isset ( $_POST ['LMI_PAYMENT_NO'] ) && preg_match ( '/^\d+$/', $_POST ['LMI_PAYMENT_NO'] ) == 1) { # Payment id

			# Request from database payment with such id
		    $payment=webmoney_merchant_pid_load($_POST['LMI_PAYMENT_NO']);
		    if(!is_array($payment)){
				  
				if($debug_errors){
				  $f=fopen("wmlog.txt", "ab");
				  fwrite($f,date("m/d/Y H:i:s", $created).' error prerequest: payment not found'."\n");
				  fclose($f);
				}

				die('NO');
			}
			
			$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));
			$presc = $currency_settings["{$payment['currency']}"]['presc'];

			# Check if payment id, purse number and ammount correspond with each other 
			if ($_POST['LMI_PAYEE_PURSE'] == $payment['payee_account'] && $_POST['LMI_PAYMENT_AMOUNT'] == round($payment['amount'], $presc)){ 

				if($debug_errors){
				  $f=fopen("wmlog.txt", "ab");
				  fwrite($f,date("m/d/Y H:i:s", $created).' OK prerequest!'."\n");
				  fclose($f);
				}

				die('YES'); # give ok to transaction

			}else{

				if($debug_errors){
				  $f=fopen("wmlog.txt", "ab");
				  fwrite($f,date("m/d/Y H:i:s", $created).' error prerequest: fake data submitted'."\n");
				  fclose($f);
				}
				die('NO');

			}

		} else { # step 3

			if($debug_errors){
				$f=fopen("wmlog.txt", "ab");
				fwrite($f,date("m/d/Y H:i:s", $created).' error prerequest: invalid payment_id submitted'."\n");
				fclose($f);
			}
			die('NO');

		}

	// ----- Real Request:
	} else {

		// check url
		$url=trim($_SERVER['REQUEST_URI'], '/');
		$alias=drupal_get_path_alias('webmoney/status');
		if($url!=$alias) die();

		
		if (isset ( $_POST ['LMI_PAYMENT_NO'] ) && 	preg_match ( '/^\d+$/', $_POST ['LMI_PAYMENT_NO'] ) == 1) { # Payment id

			# Request from database payment with such id
		    $payment=webmoney_merchant_pid_load($_POST['LMI_PAYMENT_NO']);
		    if(!is_array($payment)){
				if($debug_errors){
					$f=fopen("wmlog.txt", "ab");
					fwrite($f,date("m/d/Y H:i:s", $created).' error real request: payment not found'."\n");
					fclose($f);
				}
				die();
			} else { # If payment or items were not found

				$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));
				$presc = $currency_settings["{$payment['currency']}"]['presc'];
				
				$stored_sk=variable_get('webmoney_merchant_secretkey', '');
				$stored_sk = (!empty($stored_sk)) ? $stored_sk : $_POST['LMI_SECRET_KEY'];

				# Create check string
				$chkstring = $payment['payee_account'] .  $_POST['LMI_PAYMENT_AMOUNT'] . $payment['pid'] . $_POST['LMI_MODE'] . $_POST['LMI_SYS_INVS_NO'] . $_POST['LMI_SYS_TRANS_NO'] . $_POST['LMI_SYS_TRANS_DATE'] . $stored_sk . $_POST['LMI_PAYER_PURSE'] . $_POST['LMI_PAYER_WM'];
				
				$md5sum = strtoupper(md5($chkstring));
				$hash_check = ($_POST['LMI_HASH']==$md5sum);


				if ($_POST ['LMI_PAYMENT_NO'] == $payment['pid'] && $_POST['LMI_PAYEE_PURSE'] == $payment['payee_account'] && 
				$_POST['LMI_PAYMENT_AMOUNT'] == round($payment['amount'], $presc) && $_POST ['LMI_MODE'] == $LMI_MODE && $hash_check) { # checksum is correct, step 15

					// enroll payment
					$result = db_query('UPDATE {webmoney_merchant} SET batch=%d, payer_account=\'%s\', enrolled=%d WHERE pid=%d', $_POST['LMI_SYS_TRANS_NO'], $_POST['LMI_PAYER_PURSE'], $created, $payment['pid']);
					
					// fire hook
					$payment['batch']=$_POST['LMI_SYS_TRANS_NO'];
					$payment['payer_account']=$_POST['LMI_PAYER_PURSE'];
					$payment['enrolled']=$created;
					module_invoke_all('webmoney_merchant', 'enrolled', $payment['pid'], $payment);

					if($debug_errors){
					  $f=fopen("wmlog.txt", "ab");
					  fwrite($f,date("m/d/Y H:i:s", $created).' OK real request!'."\n");
					  fclose($f);
					}

					die();

				} else { 

					if($debug_errors){
					  $f=fopen("wmlog.txt", "ab");
					  fwrite($f,date("m/d/Y H:i:s", $created).' error real request: fake data submitted'.serialize(array($chkstring, $stored_sk, $md5sum, $_POST['LMI_HASH'], $hash_check, $_POST['LMI_PAYMENT_AMOUNT'], round($payment['amount'], $presc)))."\n");
					  fclose($f);
					}
					die();

				}

			}
		} else {
			if($debug_errors){
				$f=fopen("wmlog.txt", "ab");
				fwrite($f,date("m/d/Y H:i:s", $created).' error real request:invalid payment_id submitted'."\n");
				fclose($f);
			}
			die();
		}

	}

}