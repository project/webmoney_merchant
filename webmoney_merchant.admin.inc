<?php

function webmoney_merchant_payments_pre(){

      if (!empty($_POST['payments']) && isset($_POST['operation']) && ($_POST['operation'] == 'delete')) {
        return drupal_get_form('webmoney_merchant_payments_delete_confirm');
      }
      else {
        return drupal_get_form('webmoney_merchant_payments');
      }

}

function theme_webmoney_merchant_payments($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('ID'), 'field' => 'pid'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('User'), 'field' => 'uid'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Currency'), 'field' => 'currency'),
    array('data' => t('Memo')),
    array('data' => t('WM batch'), 'field' => 'batch'),
    array('data' => t('Payee purse'), 'field' => 'payee_account'),
    array('data' => t('Payer purse'), 'field' => 'payer_account'),
    array('data' => t('Enrolled'), 'field' => 'enrolled'),
  );

  $output = drupal_render($form['options']);
  if (isset($form['created']) && is_array($form['created'])) {
    foreach (element_children($form['created']) as $key) {
      $rows[] = array(
        drupal_render($form['payments'][$key]),
        drupal_render($form['pid'][$key]),
        drupal_render($form['created'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['amount'][$key]),
        drupal_render($form['currency'][$key]),
        drupal_render($form['memo'][$key]),
        drupal_render($form['batch'][$key]),
        drupal_render($form['payee_account'][$key]),
        drupal_render($form['payer_account'][$key]),
        drupal_render($form['enrolled'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => t('No payments available.'), 'colspan' => '11'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function webmoney_merchant_payments(&$form_state){

  $header = array(
    array(),
    array('data' => t('ID')),
    array('data' => t('ID'), 'field' => 'pid', 'sort' => 'asc'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('User'), 'field' => 'uid'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Currency'), 'field' => 'currency'),
    array('data' => t('Memo')),
    array('data' => t('WM batch'), 'field' => 'batch'),
    array('data' => t('Payee purse'), 'field' => 'payee_account'),
    array('data' => t('Payer purse'), 'field' => 'payer_account'),
    array('data' => t('Enrolled'), 'field' => 'enrolled'),
  );

  $currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));

  $sql = 'SELECT * FROM {webmoney_merchant}';
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(pid) FROM {webmoney_merchant}';
  $result = pager_query($sql, 50, 0, $query_count);

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array('delete'=>'Delete selected', 'enroll'=>'Enroll selected'),
    '#default_value' => 'delete',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  $destination = drupal_get_destination();

  $status = array(t('blocked'), t('active'));
  $payments = array();
  while ($payment = db_fetch_object($result)) {
    $payments[$payment->pid] = '';
    $form['pid'][$payment->pid] = array('#value' =>  $payment->pid);
    $form['created'][$payment->pid] = array('#value' => date("m/d/Y H:i", $payment->created));
	$form['name'][$payment->pid] = array('#value' => theme('username', user_load($payment->uid)));
    $form['amount'][$payment->pid] =  array('#value' => round($payment->amount, $currency_settings[$payment->currency]['presc']));
    $form['currency'][$payment->pid] =  array('#value' => $payment->currency);
    $form['memo'][$payment->pid] =  array('#value' => $payment->memo);
    $form['batch'][$payment->pid] =  array('#value' => $payment->batch);
    $form['payee_account'][$payment->pid] =  array('#value' => $payment->payee_account);
    $form['payer_account'][$payment->pid] =  array('#value' => ((!empty($payment->payer_account)) ? $payment->payer_account : '-'));
    $form['enrolled'][$payment->pid] =  array('#value' => (($payment->enrolled>0 && $payment->error=='') ? date("m/d/Y H:i", $payment->enrolled) : (!empty($payment->error) ? '<small><B>Error: </B>'.$payment->error.' ('.date("m/d/Y H:i", $payment->enrolled).')</smal>' : '-')));

  }
  $form['payments'] = array(
    '#type' => 'checkboxes',
    '#options' => $payments
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;

}

function webmoney_merchant_payments_validate($form, &$form_state) {
  $form_state['values']['payments'] = array_filter($form_state['values']['payments']);
  if (count($form_state['values']['payments']) == 0) {
    form_set_error('', t('No payments selected.'));
  }
}

function webmoney_merchant_payments_submit($form, &$form_state) {

  $payments = array_filter($form_state['values']['payments']);
  switch($form_state['values']['operation']){
  case 'enroll':
	  $t=time();
	  foreach ($payments as $pid){
		if(_webmoney_merchant_enrollpayment($pid, 'MANUALLY', $t))
			module_invoke_all('webmoney_merchant', 'enrolled', $pid);
	  }
	  drupal_set_message(t('The payments have been enrolled.'));
  break;
  }

}

function webmoney_merchant_payments_delete_confirm(&$form_state) {

  $edit = $form_state['post'];

  $form['payments'] = array('#tree' => TRUE);

  foreach (array_filter($edit['payments']) as $pid => $value) {

	$form['payments'][$pid] = array('#type' => 'hidden', '#value' => $pid);

  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete selected payments?'),
                      'admin/webmoney/payments', t('This action cannot be undone.'),
                      t('Delete all selected'), t('Cancel'));
}

function webmoney_merchant_payments_delete_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    
	foreach ($form_state['values']['payments'] as $pid => $value) {
		_webmoney_merchant_deletepayment($pid);
	}

	drupal_set_message(t('The payments have been deleted.'));
  
  }

  $form_state['redirect'] = 'admin/webmoney/payments';
  
  return;
}




// ---------------------------------------------------------------

function webmoney_merchant_settingsform(){

	global $base_url;

	$form=array(); 

	$form['payment_url'] = array(
		'#type' => 'textfield', 
		'#title' => t('Merchant URL'), 
		'#default_value' => variable_get('webmoney_merchant_payment_url', WEBMONEY_MERCHANT_PAYMENT_URL), 
		'#size' => 60, 
		'#maxlength' => 255, 
		'#required' => TRUE
	);

	$form['secret_key'] = array(
		'#type' => 'textfield', 
		'#title' => t('Secret Key'), 
		'#default_value' =>  variable_get('webmoney_merchant_secretkey', ''), 
		'#description' => t("Leave empty if you are going to send secret key by merchant (via https)."),
		'#size' => 40, 
		'#maxlength' => 100, 
		'#required' => FALSE
	);

	$form['result_url'] = array(
		'#type' => 'textfield', 
		'#title' => t('Result URL'), 
		'#default_value' => $base_url.'/'.drupal_get_path_alias('webmoney/status'), 
		'#description' => t("Change default value to increase security"),
		'#size' => 60, 
		'#maxlength' => 255, 
		'#required' => FALSE
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save changes'),
	);

	return $form;

}

function webmoney_merchant_settingsform_validate($form, &$form_state){
	
	global $base_url;

	if (!empty($form_state['values']['result_url'])){
		if(!preg_match("|^".$base_url."|", $form_state['values']['result_url']))
			form_set_error('result_url', t('You can not change site address, only change path.'));
    }

}

function webmoney_merchant_settingsform_submit(&$form, $form_state){

	global $base_url;

	path_set_alias('webmoney/status');

	$form_state['values']['result_url']=trim(str_replace($base_url, '', $form_state['values']['result_url']), '/');

	if($form_state['values']['result_url']!='webmoney/status') {
		path_set_alias('webmoney/status', $form_state['values']['result_url']);
	}

	variable_set('webmoney_merchant_payment_url',  $form_state['values']['payment_url']);

	variable_set('webmoney_merchant_secretkey',  $form_state['values']['secret_key']);

	drupal_set_message("Settings has been saved.");
	drupal_goto('admin/webmoney/settings');

}




// ---------------------------------------------------------------

function webmoney_merchant_currencies(){

	$default_currency_settings=_webmoney_merchant_GetDefCurSetts();

	$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize($default_currency_settings)));


	foreach($currency_settings as $key=>$value){
		
			$row=array();
  
			$row[] = array('data' => $key);
			$row[] = array('data' => ((empty($value['purse'])) ? 'n/a' : $value['purse']));
			$row[] = array('data' => (($value['enabled']==1) ? 'yes' : 'no'));
			$row[] = array('data' => l('edit', 'admin/webmoney/currencies/edit/'.$key).' '.l('delete', 'admin/webmoney/currencies/delete/'.$key).' '.l('view sample', 'admin/webmoney/sample/'.$key));
 
			$rows[] = $row;

	}
 
	// Individual table headers.
	$header = array();
	$header[] = t('Currency');
	$header[] = t('WM-purse');
	$header[] = t('Enabled');
	$header[] = t('Action');
 
	$output = theme('table', $header, $rows);

	return $output;

}


function webmoney_merchant_currency_edit($form_state, $cur=''){

		$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));

		$currency=$currency_settings[$cur];


		// interface data:

		if(!empty($cur)){

			$form['cur'] = array(
			  '#type' => 'item',
			  '#title' => t('Currency'),
			  '#value' => $cur
			);

			$form['currency'] = array(
				'#type' => 'hidden',
				'#value' => $cur,
			);


		}else{

			$form['currency'] = array(
				'#type' => 'textfield', 
				'#title' => t('Currency'), 
				'#description' => t('Enter currency ID string like "USD"'), 
				'#default_value' => '', 
				'#size' => 10, 
				'#maxlength' => 15, 
				'#required' => TRUE
			);

			$currency['purse']='';

			$currency['presc']='';

		}

	   

		$form['purse'] = array(
			'#type' => 'textfield', 
			'#title' => t('WM purse'), 
			'#default_value' => $currency['purse'], 
			'#size' => 20, 
			'#maxlength' => 15, 
			'#required' => FALSE
		);

		$form['presc'] = array(
			'#type' => 'textfield', 
			'#title' => t('Decimals'), 
			'#default_value' => $currency['presc'], 
			'#size' => 5, 
			'#maxlength' => 3, 
			'#required' => TRUE
		);

		$form['enabled'] = array(
		  '#type' => 'checkbox', 
		  '#title' => t('Enabled'),
		  '#default_value' => $currency['enabled']
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Save changes'),
		);


		return $form;

}

function webmoney_merchant_currency_edit_validate($form, &$form_state){
	

	if (empty($form_state['values']['currency'])) 
		form_set_error('', t('Currency ID does not specified.'));
	
	if($form_state['values']['enabled']=='1'){
		if(empty($form_state['values']['purse']))
			form_set_error('purse', t('WM purse can not be empty for enabled currency'));
	}

	if(!empty($form_state['values']['purse']) && !empty($form_state['values']['currency'])){

		$letter='';
		switch($form_state['values']['currency']){
		case 'USD': $letter='Z'; break;
		case 'EUR': $letter='E'; break;
		case 'RUR': $letter='R'; break;
		case 'UAH': $letter='U'; break;
		}

		if($letter!=''){

			if($letter!=substr($form_state['values']['purse'], 0, 1))
				form_set_error('purse', t('WM purse must start from !letter for !currency currency', array('!letter'=>$letter, '!currency'=>$currency)));
		}

	}


}


function webmoney_merchant_currency_edit_submit(&$form, &$form_state){
	
	$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));

	$currency_settings["{$form_state['values']['currency']}"]=array(
		'enabled' => $form_state['values']['enabled'],
		'purse' => $form_state['values']['purse'],
		'presc' =>  (int)$form_state['values']['presc']
	);

	variable_set('webmoney_merchant_currencies', serialize($currency_settings));

	drupal_set_message("Currency has been saved.");

	$form_state['redirect'] = 'admin/webmoney/currencies';

}



function webmoney_merchant_currency_delete($form_state, $cur) {

  $form['cur'] = array(
    '#type' => 'value',
    '#value' => $cur,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %cur ?', array('%cur' => $cur)),
    'admin/webmoney/currencies',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function webmoney_merchant_currency_delete_submit($form, &$form_state) {

	if ($form_state['values']['confirm']){

		$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize(_webmoney_merchant_GetDefCurSetts())));
		
		$out=array();
		foreach($currency_settings as $key=>$value){
			if($key!=$form_state['values']['cur']) $out[$key]=$value;
		}

		if(count($out)<count($currency_settings)){
			variable_set('webmoney_merchant_currencies', serialize($out));
			drupal_set_message("Currency has been deleted.");
		}
		else drupal_set_message("Currency has NOT been deleted.");


	}else drupal_set_message("Currency has NOT been deleted.");

	$form_state['redirect'] = 'admin/webmoney/currencies';

}


// ---------------------------------------------------------------


function webmoney_merchant_sample(){

	$currency_settings = unserialize(variable_get('webmoney_merchant_currencies', serialize($default_currency_settings)));

	$cur=arg(3);
	if(!empty($cur) && array_key_exists($cur, $currency_settings)) $purse=$currency_settings[$cur]['purse'];
	else $purse=$currency_settings['USD']['purse'];

  return t('<h1>Merchant purse settings</h1>
<table cellpadding="5">
<tbody><tr>
<td nowrap="nowrap">Purse:</td>

<td class="header3" align="left"><b>!purse</b></td>
<td>&nbsp;</td>
<td align="left">&nbsp;</td>
</tr>
<tr>
<td nowrap="nowrap">Trade Name:</td>
<td align="left"><input style="display: inline;" value="!tradename" size="50" id="m_name" name="m_name" type="text"></td>
<td align="center">&nbsp;-&nbsp;</td>
<td align="left"> is displayed on the web page during the payment </td>
</tr>
<tr>
<td nowrap="nowrap">Secret Key:</td>

<td align="left"><input style="display: inline;" value="!secret_key" id="secret_key" name="secret_key" size="50" type="text"></td>
<td align="center">
<input checked="checked" id="send_secret_key" name="send_secret_key" type="checkbox"> or <input id="send_secret_key1" name="send_secret_key1" type="checkbox"></td>
<td align="left">Send the Secret Key to the Result URL if the Result URL is secured</td>
</tr>
<tr>
<td nowrap="nowrap">Result URL:</td>
<td align="left"><input style="display: inline;" value="!done" id="result_url" name="result_url" size="50" maxlength="255" type="text"></td>
<td align="center">
<input checked="checked" id="send_param_prerequest" name="send_param_prerequest" value="1" type="checkbox">
or
<input id="send_param_prerequest1" name="send_param_prerequest1" value="1" type="checkbox">
</td>
<td align="left">
Send parameters in the pre-request
</td>
</tr>

<tr>
<td nowrap="nowrap">Success URL:</td>
<td align="left"><input style="display: inline;" value="!success" id="success_url" name="success_url" size="50" maxlength="255" type="text"></td>
<td align="center">
<select name="success_method" id="success_method">
<option value="2">LINK</option>
</select>
</td>
<td>
method of requesting Success URL
</td>

</tr>
<tr>
<td nowrap="nowrap">Fail URL:</td>
<td align="left"><input style="display: inline;" value="!fail" id="fail_url" name="fail_url" size="50" maxlength="255" type="text"></td>
<td align="center">
<select name="fail_method" id="fail_method">
<option value="2">LINK</option>
</select>
</td>
<td>
method of requesting Fail URL

</td>
</tr>
</tbody></table>
<table>
<tbody><tr>
<td nowrap="nowrap">
Allow overriding URL from Payment Request Form:
</td>
<td align="left">
<input id="allow_url_from_form" name="allow_url_from_form" value="1" type="checkbox">
</td>
<td></td>
<td></td>
</tr>
<tr>
<td nowrap="nowrap">
Send an error notification to merchant\'s keeper: 

</td>
<td align="left">
<input checked="checked" id="send_error" name="send_error" value="1" type="checkbox">
</td>
<td></td>
<td></td>
</tr>
<tr>
<td nowrap="nowrap">Signature algorithm:</td>
<td align="left">
<select name="auth_type" id="auth_type">
<option value="1" selected="selected">MD5</option>
</select>

</td>
<td></td>
<td></td>
</tr>
</tbody></table>',array(
     '!purse' => $purse,
     '!tradename' => variable_get('site_name', 'Drupal'),
     '!secret_key' => variable_get('webmoney_merchant_secretkey', ''),
     '!success' => url('webmoney/success',array('absolute' => TRUE)),
     '!done' => url('webmoney/status',array('absolute' => TRUE)),
     '!fail' => url('webmoney/fail',array('absolute' => TRUE)),
     ));
}